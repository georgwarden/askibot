package com.thamri.askibot.files;

public interface FilesSaver {

    String save(Object toSave);

}
