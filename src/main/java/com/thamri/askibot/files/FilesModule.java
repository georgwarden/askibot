package com.thamri.askibot.files;

import com.thamri.askibot.utils.ConfigLoader;
import dagger.Module;
import dagger.Provides;

@Module
public class FilesModule {

    @Provides
    public FilesSaver provideFilesSaver(ConfigLoader configLoader) {
        return new ImageSaver(configLoader);
    }

    @Provides
    public FilesLoader provideFilesLoader() {
        return new ImageLoader();
    }

}
