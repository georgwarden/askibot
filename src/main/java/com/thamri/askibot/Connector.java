package com.thamri.askibot;

import com.petersamokhin.bots.sdk.clients.Group;
import com.petersamokhin.bots.sdk.objects.Message;
import com.petersamokhin.bots.sdk.utils.vkapi.CallbackApiSettings;
import com.thamri.askibot.artgenerator.Art;
import com.thamri.askibot.artgenerator.ArtGenerator;
import com.thamri.askibot.connection.CommandProcessor;
import com.thamri.askibot.connection.Responder;
import com.thamri.askibot.database.Database;
import com.thamri.askibot.database.User;
import com.thamri.askibot.utils.GroupWrapper;

import javax.inject.Inject;

import static com.thamri.askibot.utils.StringDecoder.r;

public class Connector {

    @Inject public ArtGenerator artGenerator;
    @Inject public GroupWrapper groupWrapper;
    @Inject public CommandProcessor commandProcessor;
    @Inject public Responder responder;
    @Inject public Database database;

    @Inject
    public Connector() {
    }

    public void start() {

        Group group = this.groupWrapper.group();

        group.onPhotoMessage(message -> {
            Message response = new Message();
            response.to(message.authorId()).from(group);
            User user = database.getUser(message.authorId());
            System.out.println(user);
            if (user != null && user.getJoined() == 1) {
                Art art = artGenerator.generate(message);
                response.photo(art.artAsImage);
            } else {
                response.text("\u0421\u043d\u0430\u0447\u0430\u043b\u0430 \u043f\u043e\u0434\u043f\u0438\u0448\u0438\u0441\u044c ;) \u0418\u043b\u0438 \u0432\u0432\u0435\u0434\u0438 \u043a\u043e\u043c\u0430\u043d\u0434\u0443 /\u0441\u0442\u0430\u0440\u0442, \u0435\u0441\u043b\u0438 \u0442\u044b \u0443\u0436\u0435 \u043f\u043e\u0434\u043f\u0438\u0441\u0430\u043d");
            }
            response.send();
        });

        commandProcessor.process(group);

        responder.response(group);

        group.setCallbackApiSettings(new CallbackApiSettings("134.0.115.52", 25565, "/cb", true, false));
        group.onGroupJoin(event -> database.createUser(event.getInt("user_id")));
        group.onGroupLeave(event -> {
            User user = database.getUser(event.getInt("user_id"));
            user.setJoined((byte) 0);
            database.setUser(user);
            new Message().to(event.getInt("user_id")).from(group).sendVoiceMessage(this.getClass().getClassLoader().getResource("audio/da.mp3").getFile());
        });

    }

}
