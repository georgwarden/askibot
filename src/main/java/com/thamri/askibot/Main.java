package com.thamri.askibot;

import org.apache.log4j.BasicConfigurator;

public class Main {

    public static MainComponent component;

    public static void main(String... args) {
        BasicConfigurator.configure();
        component = DaggerMainComponent.create();
        component.connect().start();
    }
}
