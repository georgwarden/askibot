package com.thamri.askibot.connection;

import com.petersamokhin.bots.sdk.clients.Group;

public interface CommandProcessor {

    void process(Group group);

}
