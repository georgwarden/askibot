package com.thamri.askibot.artgenerator;

import dagger.Module;
import dagger.Provides;

import javax.inject.Named;

@Module
public class ArtModule {

    @Provides
    public ArtGenerator provideArtGenerator() {
        return new Aski();
    }

    @Provides
    public ImageRenderer provideImageRenderer() {
        return new StandardImageRenderer();
    }

    @Provides
    @Named("BasicGenerator")
    public AsciiMapGenerator provideAsciiGenerator() {
        return new AveragingAsciiMapGenerator();
    }

    @Provides
    @Named("ExperimentalGenerator")
    public AsciiMapGenerator provideExperimentalAsciiGenerator() {
        return new ExtendedAsciiMapGenerator();
    }

}
