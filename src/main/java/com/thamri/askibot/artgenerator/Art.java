package com.thamri.askibot.artgenerator;

public class Art {

    public String artAsImage;
    public String artAsText;

    public Art(String image, String text) {
        artAsImage = image;
        artAsText = text;
    }

}
