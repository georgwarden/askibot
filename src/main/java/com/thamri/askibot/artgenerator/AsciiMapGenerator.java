package com.thamri.askibot.artgenerator;

import java.awt.image.BufferedImage;

public interface AsciiMapGenerator {

    char[][] generate(BufferedImage image, int depth, boolean inverse);

}
