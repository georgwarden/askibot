package com.thamri.askibot;

import org.json.JSONObject;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class AppConfig {

    private HashMap<String, String> values;

    public AppConfig(String configPath) {
        InputStream configFile = this.getClass().getClassLoader().getResourceAsStream(configPath);
        if (configFile == null) {
            try {
                throw new IOException("Config file with such name wasn't found.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            StringBuilder col = new StringBuilder();
            Scanner r = new Scanner(configFile);
            while (r.hasNextLine()) {
                col.append(r.nextLine());
            }
            r.close();
            JSONObject config = new JSONObject(col.toString());
            values = get(config);
        }

    }

    private HashMap<String, String> get(JSONObject json) {
        HashMap<String, String> map = new HashMap<>();
        StringBuilder name = new StringBuilder();
        Object next;
        for (Object i: json.names()) {
            name.append((String) i);
            next = json.get(name.toString());
            if (next instanceof String) {
                map.put(name.toString(), (String) next);
            } else if (next instanceof JSONObject) {
                map.putAll(get((JSONObject) next, name.toString()));
            }
            name.delete(0, name.length());
        }
        return map;
    }

    private HashMap<String, String> get(JSONObject json, String prefix) {
        HashMap<String, String> map = new HashMap<>();
        StringBuilder name = new StringBuilder();
        Object next;
        for (Object i: json.names()) {
            name.append((String) i);
            next = json.get(name.toString());
            if (next instanceof String) {
                map.put(prefix + "." + name.toString(), (String) next);
            } else if (next instanceof JSONObject) {
                map.putAll(get((JSONObject) next, name.toString()));
            }
            name.delete(0, name.length());
        }
        return map;
    }

    public String getValue(String key) {
        return values.getOrDefault(key, "0");
    }

    public HashMap<String, String> getStorage() {
        return values;
    }

}
