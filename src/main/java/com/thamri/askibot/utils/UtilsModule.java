package com.thamri.askibot.utils;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class UtilsModule {

    @Provides
    public ConfigLoader provideConfigLoader() {
        return new ConfigLoader();
    }

    @Provides
    public StringsParser provideStringsParser() {
        return new StringsParser();
    }

    @Provides
    @Singleton
    public GroupWrapper provideGroupWrapper() {
        ConfigLoader configLoader = new ConfigLoader();
        return new GroupWrapper(configLoader.load("config.json"));
    }

}
