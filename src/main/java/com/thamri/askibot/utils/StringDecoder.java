package com.thamri.askibot.utils;

import java.nio.charset.Charset;

public class StringDecoder {

    public static String r(String s) {
        return new String(s.getBytes(), Charset.forName("UTF-8"));
    }

    public static String i(String s) {
        return s;
    }

}
