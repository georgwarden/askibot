package com.thamri.askibot.utils;

import com.thamri.askibot.AppConfig;

public class ConfigLoader {

    public AppConfig load(String pathInResources) {
        return new AppConfig(pathInResources);
    }

}
